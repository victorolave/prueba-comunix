<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;


class ClienteController extends Controller
{
    public function data() {
        return Cliente::all();
    }

    public function stratifiedSample(Request $request) {

        $attribute = $request['attribute'];
        $stratum = $request['stratum'];
        $sample = $request['sample'];

        $total = Cliente::all()->count();
        $stratumTotal = Cliente::where($attribute, '=', $stratum)->count();

        $stratifiedSample = round(($sample / $total) * $stratumTotal , 2);

        return $stratifiedSample;
    }

}
